package com.spikedhand.hatemachine2;

import android.os.Bundle;
import android.util.Log;
import android.app.Activity;
import android.widget.TextView;

public class InformationActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_information);
		Bundle extra = this.getIntent().getExtras();
		
		TextView lblOutput = (TextView) findViewById(R.id.lbl_pop_output);
		lblOutput.setText(lblOutput.getText().toString() + " " +extra.getInt("count"));
		
	}

}
