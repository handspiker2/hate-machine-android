package com.spikedhand.hatemachine2;

import java.io.*;

import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private Generator hate;
	private EditText txtInput;
	private TextView lblOutput;
	private RadioGroup radGroup;
	private Bundle infoBundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Preselect 4
		radGroup = (RadioGroup) findViewById(R.id.radGroup);
		radGroup.check(R.id.radFour);
		
		//Prevent Keyboard from opening
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		//amount of insults
		int amount = 0;
		if(new File(this.getFilesDir().getPath() +"/" +"insultCount").exists()){
			try{
				FileInputStream inFileStream = openFileInput("insultCount");
				// construct DataInputStream object
				DataInputStream inDataStream = new DataInputStream(inFileStream);
				
				// read the integer
				amount = inDataStream.readInt();
				
				//close the stream
				inDataStream.close();
			}catch(Exception e){
				Log.e("FILE_ACCESS", e.getMessage());
			}
		}
		//Create or grab the generator
		hate = new Generator(getResources().getStringArray(R.array.nouns),getResources().getStringArray(R.array.adj), amount);
		/*
		if(savedInstanceState != null){
			hate = savedInstanceState.getParcelable("gen");
			txtInput.setText(savedInstanceState.getString("insult"));
		}else{
			hate = new ParcelableGenerator(getResources().getStringArray(R.array.nouns),getResources().getStringArray(R.array.adj));
		}
		*/
		//Log.d("help", hate.toString());
		
		//Construct the bundle
		infoBundle = new Bundle();
		
		//Find all the views
		lblOutput = (TextView) findViewById(R.id.lblOutput);
		txtInput = (EditText) findViewById(R.id.txtInput);
		Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
		Button btnClear = (Button) findViewById(R.id.btnClear);
		Button btnAbout = (Button) findViewById(R.id.btnAbout);
		
		
		//**********************************************************Click Listeners for buttons
		
		btnSubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View src) {
				
				//close keyboard 
				hideKeyboard();
				
				if(txtInput.getText().toString().trim().equals("")){
					Toast.makeText(getApplicationContext(), R.string.errorInput, Toast.LENGTH_SHORT).show();
				}
				
				//Grab the radio button that is selected
				RadioButton radSelected = (RadioButton) findViewById(radGroup.getCheckedRadioButtonId());
				//Grab the the number that the selected radio button
				int amount = Integer.parseInt(radSelected.getText().toString());
				
				//Output the generated insult
				lblOutput.setText(
						hate.generateInsult(txtInput.getText().toString(), amount));
				
			}
		});
		
		btnClear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				txtInput.setText(null);
				lblOutput.setText(null);
			}
		});
		
		btnAbout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i("Count", "count is " + hate.getCount());
				infoBundle.putInt("count", hate.getCount());
				startActivity(new Intent("com.spikedhand.hatemachine2.INFORMATIONACTIVITY").putExtras(infoBundle));
			}
		});
	}

	public void onPause(){
		super.onPause();
		Log.d("WHERE", "in onPause");
		try {
			
			FileOutputStream outFileStream = openFileOutput("insultCount", MODE_PRIVATE);
			
			DataOutputStream outDataStream = new DataOutputStream(outFileStream);
			
			// write to the file
			Log.d("Hate" , "hate is " + hate.toString());
			outDataStream.writeInt(hate.getCount());
			
			// output done, flush out any bytes still in the stream
			outDataStream.flush();
			outDataStream.close();
		} catch (Exception e) {
			Log.d("FILE_ACCESS",e.getMessage());
		}
		Log.d("WHERE", "End of onPause");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}

	
	private void hideKeyboard(){
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtInput.getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
	
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		//savedInstanceState.putParcelable("gen",(Parcelable) hate);
		//savedInstanceState.putString("insult", txtInput.getText().toString());
	}
}
