package com.spikedhand.hatemachine2;

import java.util.Random;

import android.os.Parcel;
import android.os.Parcelable;

public class Generator{
	protected int totalCount;
	protected Random random;
	protected String[] nouns, adj;

	public Generator(String[] nouns, String[] adj){
		//Grab the words
		this.nouns = nouns;
		this.adj = adj;
		//Prepare random
		random = new Random();
		//Safety start of count 
		totalCount = 0;
	}
	
	public Generator(String[] nouns, String[] adj, int generated){
		this(nouns,adj);
		totalCount = generated;
	}
	
	public String generateInsult(String victim, int amount){
		int[] selectedWords = new int[amount];
		String output;
		if(victim.equals("") || victim == null){
			output = "You are ";
		}else{
			output= victim + " is a ";
		}
		
		
		//Make sure the amount of adjects is safe
		if(amount < 1){
			throw new IllegalArgumentException("Invalid amount (must be greater than zero)");
		}
		
		//Increase count
		totalCount++;
		
		//Generate the adj
		for(int i =0; i < selectedWords.length; i++){
			boolean copy = true;
			//check for dup numbers
			while (copy){
				//Get a random number
				selectedWords[i] = random.nextInt(adj.length);
				//Compare to all 
				for (int k = 0; k < selectedWords.length;k++){
					//check if we are use the adject already, and it never compare to itself
					if (selectedWords[i] == selectedWords[k] && k != i){
						copy = true;
						break;
					}
					//Didn't find a copy
					copy = false;
				}
			}
			//Add the adjects
			output += adj[selectedWords[i]];
			
			
			//Check for comma and "and"
			if( i == selectedWords.length -2){
				output += " and ";
			}else if ( i < selectedWords.length -1 ){
				output += ", ";
			}
			
		}
		output += " " + nouns[random.nextInt(nouns.length)] + "!";
		
		return output;
	}
	
	public int getCount(){
		return totalCount;
	}
	
}
